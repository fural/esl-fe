import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdinalPipe } from './pipes/ordinal-date.pipe';
import { IsoTimePipe } from './pipes/iso-time.pipe';

@NgModule({
  declarations: [OrdinalPipe, IsoTimePipe],
  imports: [CommonModule],
  exports: [OrdinalPipe, IsoTimePipe]
})
export class SharedModule {}
