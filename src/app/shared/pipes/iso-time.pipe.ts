import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isoTime'
})
export class IsoTimePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value && Date.parse(value)) {
      const date = new Date(value);
      const minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();

      return `${date.getHours()}:${minutes}`;
    }

    return null;
  }
}
