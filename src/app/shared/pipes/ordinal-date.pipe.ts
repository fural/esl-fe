import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'ordinalDate' })
export class OrdinalPipe implements PipeTransform {
  transform(value: string) {
    if (!value) {
      return '';
    }

    if (isNaN(Date.parse(value))) {
      return 'Invalid Date';
    }

    const date = new Date(value);

    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    const day = date.getDate();
    const dayOrdinal = this.getOrdinalSuffix(day);
    const month = months[date.getMonth()];
    const year = date.getFullYear();

    return `${day}${dayOrdinal} ${month} ${year}`;
  }

  getOrdinalSuffix(n: number) {
    const ordinals: string[] = ['th', 'st', 'nd', 'rd'];
    const value = n % 100;

    return ordinals[(value - 20) % 10] || ordinals[value] || ordinals[0];
  }
}
