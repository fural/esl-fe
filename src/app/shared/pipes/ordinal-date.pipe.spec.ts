import { OrdinalPipe } from './ordinal-date.pipe';

describe('OrdinalPipe', () => {
  let pipe: OrdinalPipe;

  beforeEach(() => {
    pipe = new OrdinalPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return the ordinal suffix name', () => {
    expect(pipe.getOrdinalSuffix(1)).toBe('st');
    expect(pipe.getOrdinalSuffix(2)).toBe('nd');
    expect(pipe.getOrdinalSuffix(3)).toBe('rd');
    expect(pipe.getOrdinalSuffix(4)).toBe('th');
  });

  it('should transform a ISO 8601 date to [day][ordinal] [Month] [Year]', () => {
    expect(pipe.transform('2018-03-21T00:00:00.000Z')).toBe('21st March 2018');
    expect(pipe.transform('2022-01-12T00:00:00.000Z')).toBe('12th January 2022');
    expect(pipe.transform('2021-09-02T00:00:00.000Z')).toBe('2nd September 2021');
    expect(pipe.transform('2019-06-23T00:00:00.000Z')).toBe('23rd June 2019');
  });

  it('should return "Invalid date" if it is not a valid ISO 8601 date', () => {
    const expected = 'Invalid Date';
    expect(pipe.transform('2019-06-233T00:00:00.000Z')).toBe(expected);
  });
});
