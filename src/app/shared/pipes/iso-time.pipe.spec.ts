import { IsoTimePipe } from './iso-time.pipe';

describe('IsoTimePipe', () => {
  let pipe: IsoTimePipe;

  beforeEach(() => {
    pipe = new IsoTimePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform a ISO 8601 date to hh:mm', () => {
    expect(pipe.transform('2019-11-01T16:38:45.450Z')).toBe('16:38');
    expect(pipe.transform('2019-11-01T12:05:00.000Z')).toBe('12:05');
    expect(pipe.transform('2019-11-01T12:51:00.000Z')).toBe('12:51');
  });
});
