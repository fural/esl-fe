export const testingLeague = {
  id: 177161,
  type: 'cup',
  name: {
    full: 'R6: Siege (PC) 5on5 Open Cup #34 - Spain',
    normal: 'R6: Siege (PC) 5on5 Open Cup #34 - Spain',
    short: 'R6: Siege (PC) 5on5 Open Cup #34 - Spain'
  },
  contestantType: 'team',
  uri: '/play/rainbowsix/europe-pc/r6siege/open/5on5-open-cup-34-spain/',
  mode: '5on5 Bomb',
  resultType: 'oneround_noshift',
  teamSize: 5,
  skillLevel: 'open',
  series: 'openspain_r6pc',
  prizePool: null,
  state: 'finished',
  timeline: {
    signUp: {
      begin: '2018-06-03T14:00:41+00:00',
      end: '2018-06-29T15:30:00+00:00'
    },
    inProgress: {
      begin: '2018-06-29T16:00:00+00:00',
      end: '2018-06-29T19:11:47+00:00'
    },
    finished: {
      begin: '2018-06-29T19:11:47+00:00'
    },
    checkIn: {
      begin: '2018-06-29T15:30:00+00:00',
      end: '2018-06-29T15:50:00+00:00'
    },
    lateSignUp: {
      begin: '2018-06-29T15:50:00+00:00',
      end: '2018-06-29T16:00:00+00:00'
    }
  },
  signOff: {
    enabled: true
  },
  tags: ['5on5', 'eu', 'europe', 'pc', 'r6siege', 'regional-eu-r6', 'rules-global', 'rules-spain', 'spain'],
  gameAccountType: 'uplay_nick',
  gameIntegration: null,
  kickBanDays: 5,
  matchSetupAllowed: true,
  matchMediaAllowed: true,
  mapvoteEnabled: true,
  anticheatEnabled: true,
  gameId: 6889,
  restrictions: [
    {
      type: 'teamSize',
      params: {
        minmembers: 5,
        maxmembers: 0
      }
    },
    {
      type: 'playerLeagueBanned',
      params: {
        leagueId: 177161
      }
    },
    {
      type: 'playerPenaltyPoints',
      params: {
        max: 11
      }
    },
    {
      type: 'alternateRoster',
      params: {
        leagueId: 177161
      }
    },
    {
      type: 'teamPenaltyPoints',
      params: {
        max: 11
      }
    },
    {
      type: 'teamLeagueBanned',
      params: {
        leagueId: 177161
      }
    },
    {
      type: 'gameaccount',
      params: {
        type: 'uplay_nick'
      }
    },
    {
      type: 'logo',
      params: {}
    },
    {
      type: 'residence_nationality',
      params: {
        countries: ['es'],
        nationalities: ['es'],
        minmembers: 3
      }
    }
  ],
  enabledFeatures: [],
  contestants: {
    signedUp: 5,
    checkedIn: 4,
    max: 4,
    maxSignedUp: null,
    maxCheckedIn: 4
  },
  signUp: {
    enabled: true,
    verificationRequired: true,
    premiumRequired: false,
    teamRequirements: {
      minMembers: 5,
      maxMembers: 0
    }
  },
  cupMode: 'single_3rd'
};
