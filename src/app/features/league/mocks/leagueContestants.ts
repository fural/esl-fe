import { Contestant } from '../interfaces/contestant';

export const testingLeagueContestants: Contestant[] = [
  {
    id: 12483873,
    seed: 0,
    status: 'signedUp',
    alias: '12483873',
    name: 'Deleted account',
    region: null
  },
  {
    id: 12274969,
    seed: 3,
    status: 'checkedIn',
    alias: '12274969',
    name: 'Arctic Gaming',
    region: 'ES'
  },
  {
    id: 12471748,
    seed: 2,
    status: 'checkedIn',
    alias: '12471748',
    name: 'KillaBeez',
    region: 'ES'
  },
  {
    id: 12091998,
    seed: 1,
    status: 'checkedIn',
    alias: '12091998',
    name: 'RedLynX',
    region: 'ES'
  },
  {
    id: 11151290,
    seed: 4,
    status: 'checkedIn',
    alias: '11151290',
    name: 'D4RK S1D3',
    region: 'ES'
  }
];
