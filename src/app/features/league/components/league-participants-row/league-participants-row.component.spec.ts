import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueParticipantsRowComponent } from './league-participants-row.component';
import { testingLeagueResults } from '../../mocks/leagueResults';

describe('LeagueParticipantsRowComponent', () => {
  let component: LeagueParticipantsRowComponent;
  let fixture: ComponentFixture<LeagueParticipantsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeagueParticipantsRowComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueParticipantsRowComponent);
    component = fixture.componentInstance;
    component.participant = testingLeagueResults[0].participants[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
