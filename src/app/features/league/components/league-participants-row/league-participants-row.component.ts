import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatchParticipant } from '../../interfaces/matchParticipant';

@Component({
  selector: 'app-league-participants-row',
  templateUrl: './league-participants-row.component.html',
  styleUrls: ['./league-participants-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeagueParticipantsRowComponent {
  @Input() participant: MatchParticipant;
  @Input() isWinner: boolean;

  constructor() {}
}
