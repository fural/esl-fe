import { Component, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { sortBy } from 'src/app/logic/sort-by';
import { MatchParticipant } from '../../interfaces/matchParticipant';

@Component({
  selector: 'app-league-participants-list',
  templateUrl: './league-participants-list.component.html',
  styleUrls: ['./league-participants-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeagueParticipantsListComponent {
  @Input() participants: MatchParticipant[];

  constructor() {}

  orderParticipantsByPlace() {
    this.participants = sortBy(this.participants, 'place');
    return this.participants || [];
  }

  trackById(index: number, participant: MatchParticipant) {
    return participant.id;
  }
}
