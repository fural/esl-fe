import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueParticipantsListComponent } from './league-participants-list.component';
import { LeagueParticipantsRowComponent } from '../league-participants-row/league-participants-row.component';
import { testingLeagueResults } from '../../mocks/leagueResults';

describe('LeagueParticipantsListComponent', () => {
  let component: LeagueParticipantsListComponent;
  let fixture: ComponentFixture<LeagueParticipantsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeagueParticipantsListComponent, LeagueParticipantsRowComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueParticipantsListComponent);
    component = fixture.componentInstance;
    component.participants = testingLeagueResults[0].participants;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
