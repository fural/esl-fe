import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LeagueResultsComponent } from './league-results.component';
import { LeagueHeaderComponent } from '../league-header/league-header.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LeagueContestantsListComponent } from '../league-contestants-list/league-contestants-list.component';
import { LeagueContestantsRowComponent } from '../league-contestants-row/league-contestants-row.component';
import { LeagueParticipantsListComponent } from '../league-participants-list/league-participants-list.component';
import { LeagueParticipantsRowComponent } from '../league-participants-row/league-participants-row.component';
import { LeagueService } from '../../services/league.service';

describe('LeagueResultsComponent', () => {
  let component: LeagueResultsComponent;
  let fixture: ComponentFixture<LeagueResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [
        LeagueResultsComponent,
        LeagueHeaderComponent,
        LeagueContestantsListComponent,
        LeagueContestantsRowComponent,
        LeagueParticipantsListComponent,
        LeagueParticipantsRowComponent
      ],
      providers: [LeagueService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async(
    inject([LeagueService], (ls: LeagueService) => {
      expect(component).toBeTruthy();
    })
  ));
});
