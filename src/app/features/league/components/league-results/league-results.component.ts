import { Component } from '@angular/core';
import { LeagueService } from '../../services/league.service';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { createEntities } from 'src/app/logic/create-entities';
import { TtmMatchResult } from '../../interfaces/ttmMatchResult';
import { Contestant } from '../../interfaces/contestant';
import { MatchParticipant } from '../../interfaces/matchParticipant';

@Component({
  selector: 'app-league-results',
  templateUrl: './league-results.component.html',
  styleUrls: ['./league-results.component.scss']
})
export class LeagueResultsComponent {
  /**
   * In a real world application, actions should be dispatched OnInit to load the data.
   * Then subscribe to the selectors.
   */
  leagues$ = forkJoin(this.ls.getLeague(), this.ls.getLeagueResults(), this.ls.getLeagueContestants()).pipe(
    map(([league, leagueResults, leagueContestants]) => {
      const leagueContestantsEntities: { [id: number]: Contestant } = createEntities(leagueContestants, {});

      /**
       * I enhanced the participants with the name of the team only for this case.
       * Normally, a good solution would be get the URL params from the router state
       * using a custom serializer, then dispatch the load actions with the selected id
       */
      leagueResults = this.enhanceParticipantsWithName(leagueResults, leagueContestantsEntities);

      return { league, leagueResults, leagueContestants };
    })
  );

  constructor(private ls: LeagueService) {}

  /**
   * Enhance the participants (MatchParticipant) with the name of the team
   * @param leagueResults the results of the league
   * @param leagueContestantsEntities entities created from a Contestant
   */
  enhanceParticipantsWithName(leagueResults: TtmMatchResult[], leagueContestantsEntities: { [id: number]: Contestant }): TtmMatchResult[] {
    return leagueResults.map((leagueResult: TtmMatchResult) => {
      const participants: MatchParticipant[] = leagueResult.participants.map(participant => {
        return { ...participant, name: leagueContestantsEntities[participant.id].name } as MatchParticipant;
      });
      return { ...leagueResult, participants };
    });
  }
}
