import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueContestantsListComponent } from './league-contestants-list.component';
import { LeagueContestantsRowComponent } from '../league-contestants-row/league-contestants-row.component';
import { LeagueParticipantsListComponent } from '../league-participants-list/league-participants-list.component';
import { LeagueParticipantsRowComponent } from '../league-participants-row/league-participants-row.component';
import { testingLeagueResults } from '../../mocks/leagueResults';
import { IsoTimePipe } from 'src/app/shared/pipes/iso-time.pipe';

describe('LeagueContestantsListComponent', () => {
  let component: LeagueContestantsListComponent;
  let fixture: ComponentFixture<LeagueContestantsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LeagueContestantsListComponent,
        LeagueContestantsRowComponent,
        LeagueParticipantsListComponent,
        LeagueParticipantsRowComponent,
        IsoTimePipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueContestantsListComponent);
    component = fixture.componentInstance;
    component.leagueResults = testingLeagueResults;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
