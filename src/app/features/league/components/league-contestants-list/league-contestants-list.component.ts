import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { TtmMatchResult } from '../../interfaces/ttmMatchResult';
import { sortBy } from 'src/app/logic/sort-by';

@Component({
  selector: 'app-league-contestants-list',
  templateUrl: './league-contestants-list.component.html',
  styleUrls: ['./league-contestants-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeagueContestantsListComponent {
  // tslint:disable-next-line:variable-name
  private _leagueResults: TtmMatchResult[];
  sortedMatches = true;

  @Input()
  set leagueResults(data: TtmMatchResult[]) {
    this._leagueResults = sortBy(data, 'beginAt');
  }

  get leagueResults(): TtmMatchResult[] {
    return this._leagueResults;
  }

  constructor() {}

  sortMatches() {
    this.leagueResults.reverse();
    this.sortedMatches = !this.sortedMatches;
  }
}
