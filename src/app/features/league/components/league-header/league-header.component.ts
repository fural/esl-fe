import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-league-header',
  templateUrl: './league-header.component.html',
  styleUrls: ['./league-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeagueHeaderComponent {
  @Input() leagueName: string;
  @Input() startDate: string;

  constructor() {}
}
