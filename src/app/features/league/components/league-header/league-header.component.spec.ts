import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueHeaderComponent } from './league-header.component';
import { OrdinalPipe } from 'src/app/shared/pipes/ordinal-date.pipe';

describe('LeagueHeaderComponent', () => {
  let component: LeagueHeaderComponent;
  let fixture: ComponentFixture<LeagueHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeagueHeaderComponent, OrdinalPipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.leagueName = 'R6: Siege (PC) 5on5 Open Cup #55 Spain';
    component.startDate = new Date().toISOString();
    expect(component).toBeTruthy();
  });
});
