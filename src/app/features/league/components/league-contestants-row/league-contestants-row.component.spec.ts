import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueContestantsRowComponent } from './league-contestants-row.component';
import { LeagueParticipantsListComponent } from '../league-participants-list/league-participants-list.component';
import { LeagueContestantsListComponent } from '../league-contestants-list/league-contestants-list.component';
import { LeagueParticipantsRowComponent } from '../league-participants-row/league-participants-row.component';
import { testingLeagueResults } from '../../mocks/leagueResults';
import { IsoTimePipe } from 'src/app/shared/pipes/iso-time.pipe';

describe('LeagueContestantsRowComponent', () => {
  let component: LeagueContestantsRowComponent;
  let fixture: ComponentFixture<LeagueContestantsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LeagueContestantsRowComponent,
        LeagueContestantsListComponent,
        LeagueParticipantsListComponent,
        LeagueParticipantsRowComponent,
        IsoTimePipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueContestantsRowComponent);
    component = fixture.componentInstance;
    component.leagueResult = testingLeagueResults[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
