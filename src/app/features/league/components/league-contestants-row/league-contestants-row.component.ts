import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { TtmMatchResult } from '../../interfaces/ttmMatchResult';

@Component({
  selector: 'app-league-contestants-row',
  templateUrl: './league-contestants-row.component.html',
  styleUrls: ['./league-contestants-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeagueContestantsRowComponent {
  @Input() leagueResult: TtmMatchResult;

  constructor() {}
}
