import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeagueResultsComponent } from './components/league-results/league-results.component';
import { LeagueHeaderComponent } from './components/league-header/league-header.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LeagueContestantsListComponent } from './components/league-contestants-list/league-contestants-list.component';
import { LeagueContestantsRowComponent } from './components/league-contestants-row/league-contestants-row.component';
import { LeagueParticipantsListComponent } from './components/league-participants-list/league-participants-list.component';
import { LeagueParticipantsRowComponent } from './components/league-participants-row/league-participants-row.component';
import { LeagueService } from './services/league.service';

@NgModule({
  declarations: [
    LeagueResultsComponent,
    LeagueHeaderComponent,
    LeagueContestantsListComponent,
    LeagueContestantsRowComponent,
    LeagueParticipantsListComponent,
    LeagueParticipantsRowComponent
  ],
  imports: [CommonModule, SharedModule],
  exports: [LeagueResultsComponent],
  providers: [LeagueService]
})
export class LeagueModule {}
