import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { testingLeague } from '../mocks/league';
import { testingLeagueResults } from '../mocks/leagueResults';
import { testingLeagueContestants } from '../mocks/leagueContestants';
import { League } from '../interfaces/league';
import { Contestant } from '../interfaces/contestant';
import { TtmMatchResult } from '../interfaces/ttmMatchResult';

/**
 * To save time dealing with CORS and proxy configurations in development mode,
 * instead of building a real service using the HttpClient,
 * I have returned dummy data as a observable for each endpoint.
 *
 * Note:
 * In the examples league ids: 177160 and 185553, there are participants ids that
 * do not match the {leagueID}/contestant response.
 */
@Injectable()
export class LeagueService {
  constructor() {}

  getLeague(): Observable<League> {
    return of(testingLeague);
  }

  getLeagueResults(): Observable<TtmMatchResult[]> {
    return of(testingLeagueResults);
  }

  getLeagueContestants(): Observable<Contestant[]> {
    return of(testingLeagueContestants);
  }
}
