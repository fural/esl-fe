import { MatchParticipant } from './matchParticipant';

export interface TtmMatchResult {
  id?: number;
  state?: string;
  bracket?: number;
  round?: number;
  position?: number;
  beginAt?: string;
  participants?: Array<MatchParticipant>;
}
