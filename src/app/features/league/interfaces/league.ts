import { LeagueName } from './leagueName';

/**
 * Seems like the ESL API documentation is not up to date,
 * e.g: the type of the id coming from the server says "id": "string",
 * and the response is "id": "number".
 */
export interface League {
  id?: number;
  name?: LeagueName;
}
