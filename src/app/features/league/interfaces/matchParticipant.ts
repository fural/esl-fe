export interface MatchParticipant {
  id?: number;
  place?: number;
  points?: Array<number>;
  name?: string;
}
