export interface LeagueName {
  full?: string;
  normal?: string;
  short?: string;
}
