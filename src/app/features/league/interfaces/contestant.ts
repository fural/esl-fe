export interface Contestant {
  id?: number;
  seed?: number;
  status?: string;
  alias?: string;
  name?: string;
  region?: string;
}
