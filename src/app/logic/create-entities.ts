export const createEntities = (array: any[], initialObj: object) => {
  return array.reduce(
    (entities: { [id: number]: any }, obj: any) => {
      return {
        ...entities,
        [obj.id]: obj
      };
    },
    {
      ...initialObj
    }
  );
};
