export const sortBy = (array: any[], property: string, order = 'asc') => {
  return (
    array &&
    array.length &&
    array.sort((a, b) => {
      return a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
    })
  );
};
